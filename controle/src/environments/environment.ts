// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {

    apiKey: 'AIzaSyBS0_U_JfKIBk615fELF_W5_4yRiObtmBs',
  
    authDomain: 'controle-ifsp-viktor.firebaseapp.com',
  
    projectId: 'controle-ifsp-viktor',
  
    storageBucket: 'controle-ifsp-viktor.appspot.com',
  
    messagingSenderId: '504533408664',
  
    appId: '1:504533408664:web:49111414760e078a72a9ed',
  
    measurementId: 'G-9EVZNNYYB3'
  
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
